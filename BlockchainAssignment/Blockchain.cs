﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    internal class Blockchain
    {
        public List<Block> Blocks = new List<Block>();

        int transactionsPerBlock = 5;

        public List<Transaction> transactionPool = new List<Transaction>();
        public Blockchain() {
            Blocks.Add(new Block());
        }

        public String blockString(int idx)
        {
            foreach (var block in Blocks)
            {
                if (block.getIndex() == idx)
                {
                   return block.ToString();
                }
            }
            return "block not found";
        }

        public List<Transaction> getPending() { 
            List<Transaction> chosen= new List<Transaction>(); 
            chosen = transactionPool.Take(transactionsPerBlock-1).ToList(); //get top 5 transactions

            transactionPool = transactionPool.Except(chosen).ToList();

            return chosen;
        }

        //get transactions with the largest fees
        public List<Transaction> getGreedyTransaction() { 
            List<Transaction> chosen = new List<Transaction>();
            for(int i =0 ; i<transactionsPerBlock; i++)
            {
                if (transactionPool.Count == 0)//if no more transactions in pool
                {
                    break;
                }
                double max = -1;
                Transaction largestFee = null; //hold transaction with largest fee
                foreach(Transaction t in transactionPool)
                {
                    if(t.fee > max)
                    {
                        max = t.fee;
                        largestFee = t;
                    }
                }
                chosen.Add(largestFee); //add to list of chosen transactions
                transactionPool.Remove(largestFee); //remove from transaction pool
            }
            
            return chosen;
        }

        //get transactions that have been waiting the longest 
        public List<Transaction> getAltruisticTransactions() { 
            List<Transaction> chosen = new List<Transaction>();
            
            for(int i = 0; i < transactionsPerBlock; i++)
            {
                if(transactionPool.Count == 0)//if no more transactions in pool
                {
                    break;
                }
                DateTime temp = DateTime.Now;
                Transaction longestTime = null;
                foreach(Transaction t in transactionPool)
                {
                    
                    if(DateTime.Compare(t.timestamp, temp) < 0)
                    {
                        temp = t.timestamp;
                        longestTime = t;
                    }
                }
                chosen.Add(longestTime);
                transactionPool.Remove(longestTime);
            }

            return chosen;
        }

        //only get transactions with preferred address
        public List<Transaction> getAddressTransactions(String target)
        {
            List<Transaction> chosen = new List<Transaction>();
            foreach(Transaction t in transactionPool)
            {
                if(chosen.Count >= transactionsPerBlock)
                {
                    return chosen;
                }
                if(t.senderAddress == target)//if wallet address is the same as target 
                {
                    chosen.Add(t);
                    transactionPool.Remove(t);
                }
            }
            
            return chosen;
        }

        //get random selection of transactions
        public List<Transaction> getRandomTransactions()
        {

            List<Transaction> chosen = new List<Transaction>();
            
            for (int i = 0; i < transactionsPerBlock; i++)
            {
                if (transactionPool.Count == 0)
                {
                    break;
                }
                Random rand = new Random();
                int randTran = rand.Next(0, transactionPool.Count);
                chosen.Add(transactionPool[randTran]);
                transactionPool.Remove(transactionPool[randTran]);
            }
            return chosen;
        }

        //sum up balance of wallet
        public double getBalance(String address)
        {
            double balance = 0;
            foreach(Block b in Blocks)
            {
                foreach(Transaction t in b.transactionList)
                {
                    //add payments received - if receiver address is wallet
                    //deduct payments made - if sender address is wallet 
                    if (t.recipientAddress.Equals(address))
                    {
                        balance += t.amount;
                    }
                    if(t.senderAddress.Equals(address))
                    {
                        balance -= (t.amount + t.fee); 
                    }
                }
            }
            return balance;
        }

        //recalculate hash and compare it against current hash
        public static bool validateHash(Block b)
        {
            String rehash = b.CreateHash();
            return rehash.Equals(b.getHash()); 
        }

        //recalculate merkle root and compare against current merkle root 
        public static bool validateMerkleRoot(Block b)
        {
            string remerkle = Block.MerkleRoot(b.transactionList);
            return remerkle.Equals(b.getMerkleRoot());
        }

        //check chain of hashs match up
        public bool validateChain()
        {
            foreach (Block b in Blocks)
            {
                if(b.getIndex() != 0)
                {
                    Block prevBlock = getBlock(b.getIndex() -1);
                    if(prevBlock.getHash() != b.getPrevHash())
                    {
                        return false;
                    } 
                }
            }
            return true;
        }

        public void addNewBlock(Block newBlock)
        {
            Blocks.Add(newBlock);
        }

        public Block getLastBlock()
        {
            return Blocks[Blocks.Count - 1];
        }

        public Block getBlock(int index)
        {
            return Blocks[index];
        }

        public String toString()
        {
            String s = "";
            foreach(Block b in Blocks)
            {
                s += b.ToString() + "\n";
            }
            return s;
        }
    }
}
