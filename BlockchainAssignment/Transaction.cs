﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    internal class Transaction
    {
        public String hash, signature;
        public String senderAddress, recipientAddress;
        public DateTime timestamp;
        public double amount, fee;

        public Transaction(String privKey, String senderAdd, String recipentAdd, double amount, double fee)
        {
            timestamp = DateTime.Now;
            senderAddress = senderAdd; 
            recipientAddress = recipentAdd;
            this.amount = amount;
            this.fee = fee;
            

            hash = CreateHash();
            signature = Wallet.Wallet.CreateSignature(senderAdd, privKey, hash);
        }

        public String CreateHash()
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all transaction properties */
            String input = timestamp + senderAddress + recipientAddress + amount + fee;

            /* Apply the hash function to the "input" string */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash;
        }

        public String ToString()
        {
            return "Transaction Hash: " + hash +
                "\nDigital Signature: " + signature +
                "\nTimestamp: " + timestamp + 
                "\nTransferred Amount: " + amount + " Coin" +
                "\nFees: " + fee +
                "\nSender Address: " + senderAddress +
                "\nReceiver Address: " + recipientAddress;
        }
    }
}
