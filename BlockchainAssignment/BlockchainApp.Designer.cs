﻿namespace BlockchainAssignment
{
    partial class BlockchainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.PrivateKeyText = new System.Windows.Forms.TextBox();
            this.PublicKeyText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GenerateWallet = new System.Windows.Forms.Button();
            this.ValidateKeys = new System.Windows.Forms.Button();
            this.CreateTransaction = new System.Windows.Forms.Button();
            this.FeeText = new System.Windows.Forms.TextBox();
            this.AmountText = new System.Windows.Forms.TextBox();
            this.ReceiverKeyText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.GenerateNewBlock = new System.Windows.Forms.Button();
            this.ReadAllBlocks = new System.Windows.Forms.Button();
            this.ReadPendingTransactions = new System.Windows.Forms.Button();
            this.ValidateBlockchain = new System.Windows.Forms.Button();
            this.CheckBalance = new System.Windows.Forms.Button();
            this.ThreadNewBlock = new System.Windows.Forms.Button();
            this.GenerateGreedyBlock = new System.Windows.Forms.Button();
            this.GenerateAltruisticBlock = new System.Windows.Forms.Button();
            this.GenerateRandomBlock = new System.Windows.Forms.Button();
            this.GenerateAddressPrefBlock = new System.Windows.Forms.Button();
            this.AddressPrefText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.richTextBox1.Location = new System.Drawing.Point(16, 15);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(875, 386);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 422);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 37);
            this.button1.TabIndex = 1;
            this.button1.Text = "Print Block";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(117, 430);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(32, 22);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // PrivateKeyText
            // 
            this.PrivateKeyText.Location = new System.Drawing.Point(315, 422);
            this.PrivateKeyText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PrivateKeyText.Name = "PrivateKeyText";
            this.PrivateKeyText.Size = new System.Drawing.Size(489, 22);
            this.PrivateKeyText.TabIndex = 4;
            // 
            // PublicKeyText
            // 
            this.PublicKeyText.Location = new System.Drawing.Point(315, 450);
            this.PublicKeyText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PublicKeyText.Name = "PublicKeyText";
            this.PublicKeyText.Size = new System.Drawing.Size(489, 22);
            this.PublicKeyText.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(233, 425);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Private Key";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(237, 454);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Public Key";
            // 
            // GenerateWallet
            // 
            this.GenerateWallet.Location = new System.Drawing.Point(811, 418);
            this.GenerateWallet.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GenerateWallet.Name = "GenerateWallet";
            this.GenerateWallet.Size = new System.Drawing.Size(81, 55);
            this.GenerateWallet.TabIndex = 8;
            this.GenerateWallet.Text = "Generate Wallet";
            this.GenerateWallet.UseVisualStyleBackColor = true;
            this.GenerateWallet.Click += new System.EventHandler(this.GenerateWallet_Click);
            // 
            // ValidateKeys
            // 
            this.ValidateKeys.Location = new System.Drawing.Point(771, 479);
            this.ValidateKeys.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ValidateKeys.Name = "ValidateKeys";
            this.ValidateKeys.Size = new System.Drawing.Size(120, 27);
            this.ValidateKeys.TabIndex = 9;
            this.ValidateKeys.Text = "Validate Keys";
            this.ValidateKeys.UseVisualStyleBackColor = true;
            this.ValidateKeys.Click += new System.EventHandler(this.ValidateKeys_Click);
            // 
            // CreateTransaction
            // 
            this.CreateTransaction.Location = new System.Drawing.Point(17, 550);
            this.CreateTransaction.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CreateTransaction.Name = "CreateTransaction";
            this.CreateTransaction.Size = new System.Drawing.Size(95, 50);
            this.CreateTransaction.TabIndex = 10;
            this.CreateTransaction.Text = "Create Transaction";
            this.CreateTransaction.UseVisualStyleBackColor = true;
            this.CreateTransaction.Click += new System.EventHandler(this.CreateTransaction_Click);
            // 
            // FeeText
            // 
            this.FeeText.Location = new System.Drawing.Point(176, 582);
            this.FeeText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FeeText.Name = "FeeText";
            this.FeeText.Size = new System.Drawing.Size(56, 22);
            this.FeeText.TabIndex = 11;
            // 
            // AmountText
            // 
            this.AmountText.Location = new System.Drawing.Point(176, 554);
            this.AmountText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AmountText.Name = "AmountText";
            this.AmountText.Size = new System.Drawing.Size(56, 22);
            this.AmountText.TabIndex = 12;
            // 
            // ReceiverKeyText
            // 
            this.ReceiverKeyText.Location = new System.Drawing.Point(332, 581);
            this.ReceiverKeyText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ReceiverKeyText.Name = "ReceiverKeyText";
            this.ReceiverKeyText.Size = new System.Drawing.Size(421, 22);
            this.ReceiverKeyText.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(139, 581);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 16);
            this.label3.TabIndex = 14;
            this.label3.Text = "Fee";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(117, 554);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "Amount";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(237, 585);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 16;
            this.label5.Text = "Receiver Key";
            // 
            // GenerateNewBlock
            // 
            this.GenerateNewBlock.Location = new System.Drawing.Point(16, 479);
            this.GenerateNewBlock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GenerateNewBlock.Name = "GenerateNewBlock";
            this.GenerateNewBlock.Size = new System.Drawing.Size(93, 52);
            this.GenerateNewBlock.TabIndex = 17;
            this.GenerateNewBlock.Text = "Generate New Block";
            this.GenerateNewBlock.UseVisualStyleBackColor = true;
            this.GenerateNewBlock.Click += new System.EventHandler(this.GenerateNewBlock_Click);
            // 
            // ReadAllBlocks
            // 
            this.ReadAllBlocks.Location = new System.Drawing.Point(157, 425);
            this.ReadAllBlocks.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ReadAllBlocks.Name = "ReadAllBlocks";
            this.ReadAllBlocks.Size = new System.Drawing.Size(75, 30);
            this.ReadAllBlocks.TabIndex = 18;
            this.ReadAllBlocks.Text = "Read All";
            this.ReadAllBlocks.UseVisualStyleBackColor = true;
            this.ReadAllBlocks.Click += new System.EventHandler(this.ReadAllBlocks_Click);
            // 
            // ReadPendingTransactions
            // 
            this.ReadPendingTransactions.Location = new System.Drawing.Point(240, 524);
            this.ReadPendingTransactions.Margin = new System.Windows.Forms.Padding(4);
            this.ReadPendingTransactions.Name = "ReadPendingTransactions";
            this.ReadPendingTransactions.Size = new System.Drawing.Size(112, 52);
            this.ReadPendingTransactions.TabIndex = 19;
            this.ReadPendingTransactions.Text = "Read Pending Transactions";
            this.ReadPendingTransactions.UseVisualStyleBackColor = true;
            this.ReadPendingTransactions.Click += new System.EventHandler(this.ReadPendingTransactions_Click);
            // 
            // ValidateBlockchain
            // 
            this.ValidateBlockchain.Location = new System.Drawing.Point(792, 519);
            this.ValidateBlockchain.Name = "ValidateBlockchain";
            this.ValidateBlockchain.Size = new System.Drawing.Size(99, 51);
            this.ValidateBlockchain.TabIndex = 20;
            this.ValidateBlockchain.Text = "Validate Blockchain";
            this.ValidateBlockchain.UseVisualStyleBackColor = true;
            this.ValidateBlockchain.Click += new System.EventHandler(this.ValidateBlockchain_Click);
            // 
            // CheckBalance
            // 
            this.CheckBalance.Location = new System.Drawing.Point(630, 478);
            this.CheckBalance.Name = "CheckBalance";
            this.CheckBalance.Size = new System.Drawing.Size(135, 29);
            this.CheckBalance.TabIndex = 21;
            this.CheckBalance.Text = "Check Balance";
            this.CheckBalance.UseVisualStyleBackColor = true;
            this.CheckBalance.Click += new System.EventHandler(this.CheckBalance_Click);
            // 
            // ThreadNewBlock
            // 
            this.ThreadNewBlock.Location = new System.Drawing.Point(122, 472);
            this.ThreadNewBlock.Name = "ThreadNewBlock";
            this.ThreadNewBlock.Size = new System.Drawing.Size(110, 66);
            this.ThreadNewBlock.TabIndex = 22;
            this.ThreadNewBlock.Text = "Thread Generate New Block";
            this.ThreadNewBlock.UseVisualStyleBackColor = true;
            this.ThreadNewBlock.Click += new System.EventHandler(this.ThreadNewBlock_Click);
            // 
            // GenerateGreedyBlock
            // 
            this.GenerateGreedyBlock.Location = new System.Drawing.Point(377, 484);
            this.GenerateGreedyBlock.Name = "GenerateGreedyBlock";
            this.GenerateGreedyBlock.Size = new System.Drawing.Size(75, 33);
            this.GenerateGreedyBlock.TabIndex = 23;
            this.GenerateGreedyBlock.Text = "Greedy";
            this.GenerateGreedyBlock.UseVisualStyleBackColor = true;
            this.GenerateGreedyBlock.Click += new System.EventHandler(this.GenerateGreedyBlock_Click);
            // 
            // GenerateAltruisticBlock
            // 
            this.GenerateAltruisticBlock.Location = new System.Drawing.Point(458, 484);
            this.GenerateAltruisticBlock.Name = "GenerateAltruisticBlock";
            this.GenerateAltruisticBlock.Size = new System.Drawing.Size(75, 33);
            this.GenerateAltruisticBlock.TabIndex = 24;
            this.GenerateAltruisticBlock.Text = "Altruistic";
            this.GenerateAltruisticBlock.UseVisualStyleBackColor = true;
            this.GenerateAltruisticBlock.Click += new System.EventHandler(this.GenerateAltruisticBlock_Click);
            // 
            // GenerateRandomBlock
            // 
            this.GenerateRandomBlock.Location = new System.Drawing.Point(539, 484);
            this.GenerateRandomBlock.Name = "GenerateRandomBlock";
            this.GenerateRandomBlock.Size = new System.Drawing.Size(75, 33);
            this.GenerateRandomBlock.TabIndex = 25;
            this.GenerateRandomBlock.Text = "Random";
            this.GenerateRandomBlock.UseVisualStyleBackColor = true;
            this.GenerateRandomBlock.Click += new System.EventHandler(this.GenerateRandomBlock_Click);
            // 
            // GenerateAddressPrefBlock
            // 
            this.GenerateAddressPrefBlock.Location = new System.Drawing.Point(377, 524);
            this.GenerateAddressPrefBlock.Name = "GenerateAddressPrefBlock";
            this.GenerateAddressPrefBlock.Size = new System.Drawing.Size(93, 46);
            this.GenerateAddressPrefBlock.TabIndex = 26;
            this.GenerateAddressPrefBlock.Text = "Address Preference";
            this.GenerateAddressPrefBlock.UseVisualStyleBackColor = true;
            this.GenerateAddressPrefBlock.Click += new System.EventHandler(this.GenerateAddressPrefBlock_Click);
            // 
            // AddressPrefText
            // 
            this.AddressPrefText.Location = new System.Drawing.Point(477, 547);
            this.AddressPrefText.Name = "AddressPrefText";
            this.AddressPrefText.Size = new System.Drawing.Size(309, 22);
            this.AddressPrefText.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(268, 484);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 32);
            this.label6.TabIndex = 28;
            this.label6.Text = "Generate Block \r\nwith Preference:";
            // 
            // BlockchainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(908, 623);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.AddressPrefText);
            this.Controls.Add(this.GenerateAddressPrefBlock);
            this.Controls.Add(this.GenerateRandomBlock);
            this.Controls.Add(this.GenerateAltruisticBlock);
            this.Controls.Add(this.GenerateGreedyBlock);
            this.Controls.Add(this.ThreadNewBlock);
            this.Controls.Add(this.CheckBalance);
            this.Controls.Add(this.ValidateBlockchain);
            this.Controls.Add(this.ReadPendingTransactions);
            this.Controls.Add(this.ReadAllBlocks);
            this.Controls.Add(this.GenerateNewBlock);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ReceiverKeyText);
            this.Controls.Add(this.AmountText);
            this.Controls.Add(this.FeeText);
            this.Controls.Add(this.CreateTransaction);
            this.Controls.Add(this.ValidateKeys);
            this.Controls.Add(this.GenerateWallet);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PublicKeyText);
            this.Controls.Add(this.PrivateKeyText);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.richTextBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "BlockchainApp";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox PrivateKeyText;
        private System.Windows.Forms.TextBox PublicKeyText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button GenerateWallet;
        private System.Windows.Forms.Button ValidateKeys;
        private System.Windows.Forms.Button CreateTransaction;
        private System.Windows.Forms.TextBox FeeText;
        private System.Windows.Forms.TextBox AmountText;
        private System.Windows.Forms.TextBox ReceiverKeyText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button GenerateNewBlock;
        private System.Windows.Forms.Button ReadAllBlocks;
        private System.Windows.Forms.Button ReadPendingTransactions;
        private System.Windows.Forms.Button ValidateBlockchain;
        private System.Windows.Forms.Button CheckBalance;
        private System.Windows.Forms.Button ThreadNewBlock;
        private System.Windows.Forms.Button GenerateGreedyBlock;
        private System.Windows.Forms.Button GenerateAltruisticBlock;
        private System.Windows.Forms.Button GenerateRandomBlock;
        private System.Windows.Forms.Button GenerateAddressPrefBlock;
        private System.Windows.Forms.TextBox AddressPrefText;
        private System.Windows.Forms.Label label6;
    }
}

