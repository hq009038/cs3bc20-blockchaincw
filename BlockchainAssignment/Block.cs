﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    internal class Block    
    {
        private DateTime timestamp;
        private int index, difficulty = 4;
        private String hash, prevHash, merkleRoot;
        private String minerAddress;
        private long nonce;
        private double reward; 

        public List<Transaction> transactionList;

        public bool threading = false; //whether should use threading or not
        public int threadNum = 2;
        private long enonce = 0;
        private CancellationToken _cancellationToken;

        public Block()
        {
            timestamp = DateTime.Now;
            index = 0;
            transactionList = new List<Transaction>();
            hash = Mine();
        }
        public Block(Block lastBlock, List<Transaction> transactions, String minerAddress, bool thread) {
            timestamp = DateTime.Now;
            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;
        
            this.minerAddress = minerAddress;
            reward = 1.0; //arbitrary fixed reward 

            transactions.Add(createRewardTransaction(transactions));
            transactionList = new List<Transaction>(transactions);

            merkleRoot = MerkleRoot(transactionList);

            this.threading = thread;
            
            if (threading)
            {  
                hash = ThreadedMine();
                
            }
            else
            {
                hash = Mine();
            }
            
        }

        public String CreateHash()
        {
            SHA256 hasher = SHA256Managed.Create();

            String input = index.ToString() + timestamp.ToString() + prevHash + nonce + enonce + merkleRoot;
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));
            
            String hash = string.Empty;
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);
            
            return hash;
        }

        public String CreateHash(int enonce, int nonce)
        {
            SHA256 hasher = SHA256Managed.Create();

            String input = index.ToString() + timestamp.ToString() + prevHash + nonce + enonce + merkleRoot;
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            String hash = string.Empty;
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash;
        }

        public Transaction createRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Sum(i => i.fee);
            return new Transaction("", "Mine Rewards", minerAddress, (reward + fees), 0);
        }

        public String Mine()
        {
            nonce = 0;
            String hash = CreateHash();

            String pre = new string('0', difficulty);

            while (!hash.StartsWith(pre))
            {
                nonce++;
                hash = CreateHash();
            }

            return hash;
        }

        public String ThreadedMine()
        {
            //stops threads if another has found an acceptable hash
            var cancellationSource = new CancellationTokenSource();
            this._cancellationToken = cancellationSource.Token;

            //thread safe local variables
            ThreadLocal<String> localHash = new ThreadLocal<string>(() => { return ""; } );
            ThreadLocal<int> localNonce = new ThreadLocal<int>(() => { return 0; } );
            String result = "";
            int threadNonce = 0;
            int threadID = 0;

            int threadsNo = this.threadNum; //number of threads to create

            String pre = new string('0', difficulty);

            
            Task[] ts = new Task[threadsNo]; //create threads 
            for(int i = 0; i<threadsNo; i++)
            {
                ts[i] = Task.Run(() => //each thread runs following function
                {
                    while(!_cancellationToken.IsCancellationRequested) //uncitl a cancellation is requested
                    {
                        localNonce.Value++; //increment thread nonce
                        localHash.Value = CreateHash(Thread.CurrentThread.ManagedThreadId, localNonce.Value); //generate hash with thread id
                                                                    //so hash is still unique with threads using the same nonce
                        if (localHash.Value.StartsWith(pre))//if hash meets PoW
                        {
                            cancellationSource.Cancel(); //request cancellation
                            result = localHash.Value; //store values locally
                            threadNonce = localNonce.Value;
                            threadID = Thread.CurrentThread.ManagedThreadId;
                            
                        }
                    }
                });
            }
            Task.WaitAll(ts); //wait until all threads have finished
            String hash = result; //assign values to block
            this.nonce = threadNonce;
            this.enonce = threadID;
            return hash;

        }

        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList(); // Get a list of transaction hashes for "combining"

            // Handle Blocks with...
            if (hashes.Count == 0) // No transactions
            {
                return String.Empty;
            }
            if (hashes.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.CombineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<String> merkleLeaves = new List<String>(); // Keep track of current "level" of the tree

                for (int i = 0; i < hashes.Count; i += 2) // Step over neighbouring pair combining each
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i + 1])); // Hash neighbours leaves
                    }
                }
                hashes = merkleLeaves; // Update the working "layer"
            }
            return hashes[0]; // Return the root node
        }

        public String ToString()
        {
            String str = "Index: " + index + "\t Timestamp: " + timestamp + 
                "\nHash: " + hash + "\n Previous Hash: " + prevHash + 
                "\nDifficulty: " + difficulty + " Nonce: " + nonce + " E-Nonce: " + enonce +
                "\nReward: " + reward + "\nMiner Address: " + minerAddress +
                "\nTransactions: " + transactionList.Count() + "\n";
            
            foreach (Transaction t in transactionList)
            {
                str += "\n" + t.ToString() + "\n";
            }
            str += "\nMerkle Root: " + merkleRoot;

            str += "\n";

            return str;
        }

        public int getIndex()
        {
            return index;
        }

        public String getHash()
        {
            return hash;
        }
        public String getPrevHash()
        {
            return prevHash;
        }

        public String getMerkleRoot()
        {
            return merkleRoot;
        }
    }
}
