﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlockchainAssignment.Wallet; 

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        Blockchain blockchain;
        public BlockchainApp()
        {
            InitializeComponent();
            blockchain = new Blockchain();
            richTextBox1.Text = "New Blockchain Initialised";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.Text = blockchain.blockString(Convert.ToInt32(textBox1.Text));
            }
            catch
            {
                richTextBox1.Text = "please input a number";
            }
        }

        //generate new wallet
        private void GenerateWallet_Click(object sender, EventArgs e)
        {
            Wallet.Wallet wallet = new Wallet.Wallet(out string privKey);
            PrivateKeyText.Text = privKey;
            PublicKeyText.Text = wallet.publicID;
        }

        //validates that the keys in the text boxes are relataed
        private void ValidateKeys_Click(object sender, EventArgs e)
        {
            if(Wallet.Wallet.ValidatePrivateKey(PrivateKeyText.Text, PublicKeyText.Text) == true)
            {
                richTextBox1.Text = ("Keys are valid");
            }
            else
            {
                richTextBox1.Text = ("Keys are invalid");
            }
        }

        //create a transaction to put in the pool
        private void CreateTransaction_Click(object sender, EventArgs e)
        {
            //check wallet has enough funds to make transaction
            if (blockchain.getBalance(PublicKeyText.Text) >= Convert.ToSingle(AmountText.Text))
            {
                Transaction transaction = new Transaction(PrivateKeyText.Text, PublicKeyText.Text, ReceiverKeyText.Text, 
                    Convert.ToDouble(AmountText.Text), Convert.ToDouble(FeeText.Text));
                //validate the transaction's signature before adding to transaction pool
                if (Wallet.Wallet.ValidateSignature(PublicKeyText.Text, transaction.hash, transaction.signature))
                {
                    blockchain.transactionPool.Add(transaction);//add transaction to pool
                    richTextBox1.Text = transaction.ToString(); //ouput transaction
                }
                else
                {
                    richTextBox1.Text = "Transaction failed - invalid signature";//error message
                }
            }
            else
            {
                richTextBox1.Text = "Transaction failed - insufficient funds";//error message
            }
        }

        private void GenerateNewBlock_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getPending();

            DateTime startTime = DateTime.Now;
            Block newBlock = new Block(blockchain.getLastBlock(), transactions, PublicKeyText.Text, false);
            DateTime endTime = DateTime.Now;
            TimeSpan timeTaken = endTime - startTime;
            System.Diagnostics.Debug.WriteLine("Time taken : " + timeTaken);

            blockchain.addNewBlock(newBlock);

            richTextBox1.Text = blockchain.ToString() + "\n\nThreads: 1\n Time taken :" + timeTaken;
        }

        private void ReadAllBlocks_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = blockchain.toString();
        }

        private void ReadPendingTransactions_Click(object sender, EventArgs e)
        {
            String str = "";
            foreach (Transaction t in blockchain.getPending())
            {
                str += "\n" + t.ToString() + "\n";
            }
            richTextBox1.Text = str;
        }

        private void ValidateBlockchain_Click(object sender, EventArgs e)
        { 
            if (blockchain.Blocks.Count == 1) //if only genesis block
            {
                if (!Blockchain.validateHash(blockchain.Blocks[0])) // Recompute Hash to check validity
                    richTextBox1.Text = "Blockchain is invalid";
                else
                    richTextBox1.Text = "Blockchain is valid";
                return;
            }

            for (int i = 1; i < blockchain.Blocks.Count - 1; i++) //check through chain of blocks
            {
                if (
                    blockchain.Blocks[i].getPrevHash() != blockchain.Blocks[i - 1].getHash() || // Check hash "chain"
                    !Blockchain.validateHash(blockchain.Blocks[i]) ||  // Check each blocks hash
                    !Blockchain.validateMerkleRoot(blockchain.Blocks[i]) // Check transaction integrity using Merkle Root
                )
                {
                    richTextBox1.Text = "Blockchain is invalid";
                    return;
                }
            }
            richTextBox1.Text = "Blockchain is valid";
        }
    
        //get wallet balance and output on screen
        private void CheckBalance_Click(object sender, EventArgs e)
        {
            double balance = blockchain.getBalance(PublicKeyText.Text);
            richTextBox1.Text = "Wallet balance: " + balance;
        }

        private void ThreadNewBlock_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getPending();

            DateTime startTime = DateTime.Now;
            Block newBlock = new Block(blockchain.getLastBlock(), transactions, PublicKeyText.Text, true);
            DateTime endTime = DateTime.Now;
            TimeSpan timeTaken = endTime - startTime;
            System.Diagnostics.Debug.WriteLine("Time taken : " + timeTaken);

            blockchain.addNewBlock(newBlock);

            richTextBox1.Text = blockchain.ToString() + "\n\nThreads: " + newBlock.threadNum + "\nTime taken :" + timeTaken;
        }

        private void GenerateGreedyBlock_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getGreedyTransaction();

            Block newBlock = new Block(blockchain.getLastBlock(), transactions, PublicKeyText.Text, false);
            
            blockchain.addNewBlock(newBlock);

            richTextBox1.Text = blockchain.ToString();
        }

        private void GenerateAltruisticBlock_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getAltruisticTransactions();

            Block newBlock = new Block(blockchain.getLastBlock(), transactions, PublicKeyText.Text, false);

            blockchain.addNewBlock(newBlock);

            richTextBox1.Text = blockchain.ToString();
        }

        private void GenerateRandomBlock_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getRandomTransactions();

            Block newBlock = new Block(blockchain.getLastBlock(), transactions, PublicKeyText.Text, false);

            blockchain.addNewBlock(newBlock);

            richTextBox1.Text = blockchain.ToString();
        }

        private void GenerateAddressPrefBlock_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getAddressTransactions(AddressPrefText.Text);

            Block newBlock = new Block(blockchain.getLastBlock(), transactions, PublicKeyText.Text, false);

            blockchain.addNewBlock(newBlock);

            richTextBox1.Text = blockchain.ToString();
        }
    }
}
